#ifndef GEMM_H
#define GEMM_H

/*
 * c=a*b in row major
 * a: m by k
 * b: k by n
 * c: m by n
 * assuming m k n divisible by block_size
 */
template <typename T=float,unsigned block_size=16>
__global__ void gemm_kernel0(const T *a,unsigned m,const T *b,unsigned k,unsigned n,T *c){
    __shared__ T as[block_size][block_size];
    __shared__ T bs[block_size][block_size];
    
    unsigned row=blockIdx.y*block_size+threadIdx.y;
    unsigned col=blockIdx.x*block_size+threadIdx.x;
    
    if(row<m && col<n){
        T sum=0;
        for(unsigned ik=0; ik<k; ik+=block_size){
            as[threadIdx.y][threadIdx.x]=a[row*k+ik+threadIdx.x];
            bs[threadIdx.y][threadIdx.x]=b[(ik+threadIdx.y)*n+col];
            __syncthreads();
            for(unsigned i=0; i<block_size; i++) sum+=as[threadIdx.y][i]*bs[i][threadIdx.x];
            __syncthreads();
        }
        
        c[row*n+col]=sum;
    }
    return;
}

template <typename T=float,unsigned block_size=16>
cudaError_t gemm0(const T *a,unsigned m,const T *b,unsigned k,unsigned n,T *c){
    dim3 block(block_size,block_size);
    dim3 grid(n/block_size,m/block_size);
    gemm_kernel0<T,block_size><<<grid,block>>>(a,m,b,k,n,c);
    return cudaGetLastError();
}

template <typename T=float,unsigned tile_size=32,unsigned block_size=16,unsigned thread_tile_size=2>
__global__ void gemm_kernel(const T *a,unsigned m,const T *b,unsigned k,unsigned n,T *c){
    __shared__ T as[tile_size][tile_size];
    __shared__ T bs[tile_size][tile_size];

    T col_a[thread_tile_size];
    T row_b[thread_tile_size];
    T ab[thread_tile_size][thread_tile_size];

    for(unsigned i=0; i<thread_tile_size; i++){
        for(unsigned j=0; j<thread_tile_size; j++) ab[i][j]=0;
    }
    
    unsigned base_row=blockIdx.y*tile_size;
    unsigned base_col=blockIdx.x*tile_size;
    unsigned block_base_row=threadIdx.y*thread_tile_size;
    unsigned block_base_col=threadIdx.x*thread_tile_size;
    unsigned thread_base_row=base_row+block_base_row;
    unsigned thread_base_col=base_col+block_base_col;
    
    if(base_row<m && base_col<n){
        for(unsigned ik=0; ik<k; ik+=tile_size){
            for(unsigned i=0; i<tile_size; i+=block_size){
                unsigned yi=threadIdx.y+i;
                for(unsigned j=0; j<tile_size; j+=block_size){
                    unsigned xj=threadIdx.x+j;
                    as[yi][xj]=a[(base_row+yi)*k+ik+xj];
                    bs[yi][xj]=b[(ik+yi)*n+base_col+xj];
                }
            }
            __syncthreads();

            for(unsigned it=0; it<tile_size; it++){
                for(unsigned i=0; i<thread_tile_size; i++){
                    col_a[i]=as[block_base_row+i][it];
                    row_b[i]=bs[it][block_base_col+i];
                }
                for(unsigned i=0; i<thread_tile_size; i++){
                    for(unsigned j=0; j<thread_tile_size; j++) ab[i][j]+=col_a[i]*row_b[j];
                }
            }
            __syncthreads();
        }
        
        for(unsigned i=0; i<thread_tile_size; i++){
            unsigned row=thread_base_row+i;
            for(unsigned j=0; j<thread_tile_size; j++){
                unsigned col=thread_base_col+j;
                c[row*n+col]=ab[i][j];
            }
        }
    }
    return;
}

template <typename T=float,unsigned tile_size=32,unsigned block_size=16,unsigned thread_tile_size=2>
cudaError_t gemm(const T *a,unsigned m,const T *b,unsigned k,unsigned n,T *c){
    dim3 block(block_size,block_size);
    dim3 grid(n/tile_size,m/tile_size);
    gemm_kernel<T,tile_size,block_size,thread_tile_size><<<grid,block>>>(a,m,b,k,n,c);
    return cudaGetLastError();
}

#endif
