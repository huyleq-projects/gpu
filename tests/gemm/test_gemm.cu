#include <cblas.h>

#include "myio.h"
#include "mylib.h"

#include "helper_cuda.h"
#include "gemm.h"

int main(int argc,char **argv){
	myio_init(argc,argv);
	
	unsigned m,k,n; get_param("m",m,"k",k,"n",n);
	unsigned mk=m*k, kn=k*n, mn=m*n;

	float *h_a=new float[mk]; rand(h_a,mk);
	float *h_b=new float[kn]; rand(h_b,kn);
	float *h_c=new float[mn]; 

	cblas_sgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,m,n,k,1,h_a,k,h_b,n,1,h_c,m);

	float *d_a, *d_b, *d_c;
	checkCudaErrors(cudaMalloc(&d_a,mk*sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_a,h_a,mk*sizeof(float),cudaMemcpyHostToDevice));

	checkCudaErrors(cudaMalloc(&d_b,kn*sizeof(float)));
	checkCudaErrors(cudaMemcpy(d_b,h_b,kn*sizeof(float),cudaMemcpyHostToDevice));
	
	checkCudaErrors(cudaMalloc(&d_c,mn*sizeof(float)));
	
	checkCudaErrors(gemm<>(d_a,m,d_b,k,n,d_c));
	
    float *t_c=new float[mn]; 
	checkCudaErrors(cudaMemcpy(t_c,d_c,mn*sizeof(float),cudaMemcpyDeviceToHost));
	printf("error=%g\n",relative_l2_error(h_c,t_c,mn));

	delete []h_a; delete []h_b; delete []h_c; delete []t_c;
	checkCudaErrors(cudaFree(d_a));
	checkCudaErrors(cudaFree(d_b));
	checkCudaErrors(cudaFree(d_c));

	myio_close();
	return 0;
}

